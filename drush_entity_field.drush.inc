<?php

/**
 * @file
 * drush_entity_field.drush.inc
 *
 * Drush support for field CRUD operations over entities.
 */

/**
 * Implements hook_drush_help().
 */
function drush_entity_field_drush_help($section) {
  switch ($section) {
    case 'drush:entity-field-create':
      return dt('Create a new field instance and attach it to a bundle.');

    case 'drush:entity-field-update-definition':
      return dt('Update a field definition. Only cardinality and translatable can be changed.');

    case 'drush:entity-field-update-instance':
      return dt('Update a field instance. Bundle is required.');

    case 'drush:entity-field-delete':
      return dt('Delete a field: its definition and all its instances.');

    case 'drush:entity-field-list':
      return dt('List all the available fields and widgets.');

    case 'drush:entity-field-process-excel':
      return dt('Read an Excel file, creating the fields included in it. See README.txt for the required Excel file format.');

    case 'drush:entity-field-create-vocabulary':
      return dt('Create a vocabulary if it does not exist.');
  }
}

/**
 * Implements hook_drush_command().
 */
function drush_entity_field_drush_command() {
  $items = array();
  // Create field.
  $items['entity-field-create'] = array(
    'callback' => 'drush_entity_field_create',
    'description' => dt('Create a new field instance and attach it to a bundle. Arguments must be provided separated by commas without spaces, in the following order:'),
    'arguments' => array(
      'field name' => dt('Field name. REQUIRED. Must be unique over active and disabled fields. Cannot contain invalid characters. Cannot be longer than 32 characters.'),
      'field type' => dt('Field type. REQUIRED. See list: https://groups.drupal.org/node/124289'),
      'field label' => dt('Field label. REQUIRED. Enclose in quoutes if it contains blank spaces.'),
      'field widget' => dt('Field widget. REQUIRED. Run command "efl" to show all available.'),
      '' => dt('The following params are optional, but all of them must be specified in this order if any of them is needed.'),
      'cardinality' => dt('Field cardinality. Specify -1 for Unlimited. Default: 1.'),
      'required' => dt('Field is required. Specify 1 if desired. Default: 0.'),
      'translatable' => dt('Field is translatable. Specify 1 if desired. Default: 0.'),
    ),
    'options' => array(
      'entity' => dt('Entity type for the bundle. Optional. Default: node.'),
      'bundle' => dt('Bundle. REQUIRED.'),
      'vocab'  => dt('Vocabulary. Only applies to taxonomy terms. Mandatory for them.'),
      'check' => dt('Check only. Check parameters but do not perform any database operation.'),
    ),
    'examples' => array(
      'drush efc field_subject,text -bundle=article' => 'Add a text field to the bundle Article, using default values.',
      '' => '',
      'drush efc field_contact_name,text,"Name contact",text_textfield,-1,1,0 --entity=personas --bundle=contact' => 'Full field specification.',
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'aliases' => array('efc'),
  );

  // Update field definition.
  $items['entity-field-update-definition'] = array(
    'callback' => 'drush_entity_field_update_definition',
    'description' => dt('Update a field definition. Only cardinality and translatable properties can be changed. Arguments must be provided separated by commas without spaces, in the following order:'),
    'arguments' => array(
      'field name' => dt('Field name. REQUIRED. Field must exist.'),
      'cardinality' => dt('Field cardinality. Optional. Specify a number or -1 for Unlimited. Leave it blank for not updating.'),
      'translatable' => dt('Field is translatable. Optional. Specify 1 or 0. Leave it blank for not updating.'),
    ),
    'options' => array(
      'check' => dt('Check only. Check parameters but do not perform any database operation.'),
    ),
    'examples' => array(
      'drush efud field_name,-1,0' => 'Change field_name to unlimited cardinality and not translatable.',
      '' => '',
      'drush efud field_name,,1' => 'Change field_name to translatable. Cardinality is not changed.',
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'aliases' => array('efud'),
  );

  // Update field instance.
  $items['entity-field-update-instance'] = array(
    'callback' => 'drush_entity_field_update_instance',
    'description' => dt('Update a field instance. Bundle is required. Arguments must be provided separated by commas without spaces, in the following order:'),
    'arguments' => array(
      'field name' => dt('Field name. REQUIRED. Field must exist.'),
      'field widget' => dt('Field widget. Optional. It must be suitable for the field type. Leave it blank for not updating.'),
      'required' => dt('Field is required. Optional. Specify 1 or 0. Leave it blank for not updating.'),
      'field label' => dt('Field label. Optional. Enclose in quoutes if it contains blank spaces. Leave it blank for not updating.'),
      'field description' => dt('Field description. Optional. Enclose in quoutes if it contains blank spaces. Leave it blank for not updating.'),
    ),
    'options' => array(
      'entity' => dt('Entity type for the bundle. Optional. Default: node.'),
      'bundle' => dt('Bundle. REQUIRED.'),
      'check' => dt('Check only. Check parameters but do not perform any database operation.'),
    ),
    'examples' => array(
      'drush efui field_name,options_buttons,true,"New label", "New description."' => 'Change field widget, make it required, and change its label and description.',
      '' => '',
      'drush efui field_name,,,,"New description."' => 'Change only field description.',
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'aliases' => array('efui'),
  );

  // Delete field.
  $items['entity-field-delete'] = array(
    'callback' => 'drush_entity_field_delete',
    'description' => dt('Delete a field: its definition and all its instances.'),
    'arguments' => array(
      'field name' => dt('Field name. REQUIRED.'),
    ),
    'options' => array(
      'check' => dt('Check only. Check parameters but do not perform any database operation.'),
    ),
    'examples' => array(
      'drush efd field_name' => 'Delete field_name and all its instances. ',
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'aliases' => array('efd'),
  );

  // List fields.
  $items['entity-field-list'] = array(
    'callback' => 'drush_entity_field_list',
    'description' => dt('List all the available fields and widgets.'),
    'examples' => array(
      'drush efl' => 'List all the available fields and widgets.',
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'aliases' => array('efl'),
  );

  // Read an Excel file, creating all the fields included in it.
  $items['entity-field-process-excel'] = array(
    'callback' => 'drush_entity_field_process_excel',
    'description' => dt('Read an Excel file, creating all the fields included in it. PHPExcel library is required. The Excel file must have an specific format. Use efx_sample.xls as a model.'),
    'arguments' => array(
      'Excel filename' => dt('Full path to the Excel file to process. REQUIRED. File must exist and with read permission.'),
    ),
    'options' => array(
      'check' => dt('Check only. Check parameters but do not perform any database operation.'),
      'PHPExcel' => dt('Path to PHPExcel Library (no trailing slash). Optional. Default: sites/all/libraries/PHPExcel.'),
    ),
    'examples' => array(
      'drush efx ./fields.xlsx' => 'Read the fields.xlsx file and create the fields included in it.',
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'aliases' => array('efx'),
  );

  $items['entity-field-create-vocabulary'] = array(
    'callback' => 'drush_entity_field_create_vocabulary',
    'description' => dt('Create a vocabulary if it does not exist.'),
    'arguments' => array(
      'voc_name' => dt('List of valid vocabulary names.'),
    ),
    'examples' => array(
      'drush efcv voc_name' => 'Create a new vocabulary named voc_name.',
    ),
    'options' => array(
      'check' => dt('Check only. Check parameters but do not perform any database operation.'),
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'aliases' => array('efcv'),
  );
  return $items;
}

/**
 * Create a field into an entity/bundle pair.
 *
 * drush efc field_contact_name,text,Name,text_textfield,-1,true
 *   --entity=personas --bundle=contact
 *
 * @TODO:
 *     - Entityreference target entity selection
 */
function drush_entity_field_create($entity_type = '', $bundle = '') {

  $entity_type = drush_get_option('entity', '');
  $bundle = drush_get_option('bundle', '');
  $vocab = drush_get_option('vocab', '');
  $check_only = drush_get_option('check', FALSE);

  // Check that entity type exists, if it is specified
  if (!empty($entity_type)) {
    $entity = entity_get_info($entity_type);
    if (empty($entity)) {
      drush_log('Error: Entity type ' . $entity_type . ' does not exist!', 'error');
      return;
    }
    else {
      drush_log('Info: Using entity ' . $entity_type, 'ok');
    }
  }

  // Check bundle argument, if it is specified.
  if (!empty($bundle)) {
    // Check that bundle exists.
    if (!array_key_exists($bundle, $entity['bundles'])) {
      drush_log('Error: ' . $bundle . ' does not exist!', 'error');
      return;
    }
    else {
      drush_log('Info: Using bundle ' . $bundle, 'ok');
    }
  }

  $args = func_get_args();

  // Iterate over each field spec.
  foreach ($args as $string) {

    list($name, $type, $label, $widget, $cardinality, $required, $translatable) = explode(',', $string);

    // Check if field already exists.
    $info = field_info_field($name);
    if (empty($info)) {
      // Field does not exist already. Create it.
      $field = array(
        'field_name' => $name,
        'type' => $type,
        'cardinality' => (is_numeric($cardinality) && intval($cardinality) >= -1) ? intval($cardinality) : 1,
        'translatable' => (is_numeric($translatable)) ? $translatable : 0,
      );

      // Check that $type is defined.
      $field_types_list = field_info_field_types();
      if (!array_key_exists($type, $field_types_list)) {
        drush_log('Error: field type ' . $type . ' does not exist. Field ' .
          $name . ' not created.', 'error');
        return;
      }

      // If taxonomy term and vocabulary is provided, check that vocabulary exists.
      if ($type == 'taxonomy_term_reference') {
        if (!empty($vocab)) {
          $voc = taxonomy_vocabulary_machine_name_load($vocab);
          if (!is_object($voc)) {
            drush_log('Warning: vocabulary ' . $vocab . ' does not exist.',
              'warning');
            if (drush_confirm('Do you want to create and use a new vocabulary named ' .
              $vocab . '?')) {
                $vocabulary = (object) array(
                  'name' => $vocab,
                  'description' => $vocab,
                  'machine_name' => $vocab,
                );
                if (!$check_only) {
                  taxonomy_vocabulary_save($vocabulary);
                }
            }
            else {
              drush_log('Error: field ' . $name . ' not created.', 'error');
              return;
            }
          }

          // Set the vocabulary
          $field['settings'] = array(
            'allowed_values' => array(
              array(
                'vocabulary' => $vocab,
                'parent' => 0,
              ),
            ),
          );
        }
        else {
          drush_log('Vocabulary must be supplied when creating taxonomy fields: --vocab=<vocab_name>', 'error');
          return;
        }
      }

      // Field creation.
      if (!$check_only) {
        try {
          drush_op('field_create_field', $field);
        }
        catch (Exception $e) {
          drush_log('Error: Field *' . $name . '* of type *' . $type .
            '* with cardinalilty *' . $cardinality .
            '* can not be created on Bundle *' . $bundle . '* !'. PHP_EOL .
            $e->getMessage(), 'error');
          return DRUSH_APPLICATION_ERROR;
        }

        drush_log('Field *' . $name . '* of type *' . $type . '* with cardinalilty *' .
          $cardinality . '* successfully created.', 'ok');
      }
    }

    // If entity and bundle are not specified, the field is created,
    // but no field instance will be attached to any bundle.
    if (empty($entity_type) || empty($bundle)) {
      continue;
    }

    // Check that the widget is defined.
    if (empty($label) || empty($widget)) {
      drush_log('Error: label and widget for field ' . $name .
        ' are both required. Run "drush efl" to find out the available widgets for the field type. Field ' . $name . ' not created.', 'error');
      return;
    }
    $info = field_info_widget_types($widget);
    if (empty($info)) {
      drush_log('Error: widget ' . $widget . ' is not defined for field type ' . $type . ' Field ' . $name . ' not created.', 'error');
      return;
    }

    // Create the instance.
    $instance = array(
      'field_name' => $name,
      'entity_type' => $entity_type,
      'bundle' => $bundle,
      'label' => dt($label),
      'required' => (is_numeric($required)) ? $required : 0,
      'widget' => array('type' => $widget),
    );

    if (!$check_only) {
      try {
        drush_op('field_create_instance', $instance);
      }
      catch (Exception $e) {
        drush_log('Error: Field instance *' . $name . '* of type *' . $type .
           '* with label *' . $label . '* can not be created on Bundle *' .
            $bundle . '* !' . PHP_EOL . $e->getMessage(), 'error');
        return DRUSH_APPLICATION_ERROR;
      }
      drush_log('Field instance *' . $name . '* of type *' . $type .
        '* with label *' . $label . '* successfully created on Bundle *' .
        $bundle . '*.', 'ok');
    }
    else {
      drush_log('Check mode: arguments are correct but no operation was performed.', 'ok');
    }

    $urls[] = url(base_path() . drush_field_ui_bundle_admin_path($entity_type, $bundle) . '/fields/' . $name, array('absolute' => TRUE));
  }

  if (!empty($urls)) {
    drush_print(implode(' ', $urls));
  }
}

/**
 * Update an existing field definition.
 *
 * drush efud field_name
 */
function drush_entity_field_update_definition() {

  $check_only = drush_get_option('check', FALSE);

  $args = func_get_args();

  // Iterate over each field spec.
  foreach ($args as $string) {

    list($name, $cardinality, $translatable) = explode(',', $string);

    // Check if field exists.
    $field = field_info_field($name);
    if (empty($field)) {
      drush_log('Error: Field ' . $name . ' does not exist.', 'error');
      return;
    }

    // It is not necessary to pass in a fully populated $field structure,
    // just the data to be updated.
    // Please note that any module may forbid any update for any reason.
    if (is_numeric($cardinality) && intval($cardinality) >= -1) {
      $field['cardinality'] = intval($cardinality);
    }
    if (is_numeric($translatable)) {
      $field['translatable'] = $translatable;
    }

    if (!$check_only) {
      try {
        drush_op('field_update_field', $field);
      }
      catch (Exception $e) {
        drush_log('Error: Field definition *' . $name . '* can not be updated!' .
          PHP_EOL . $e->getMessage(), 'error');
        return DRUSH_APPLICATION_ERROR;
      }

      drush_log('Field definition *' . $name . '* successfully updated.', 'ok');
    }
    else {
      drush_log('Check mode: arguments are correct but no operation was performed.', 'ok');
    }
  }
}

/**
 * Update an existing field instance.
 *
 * drush efui field_name
 */
function drush_entity_field_update_instance() {

  $entity_type = drush_get_option('entity', 'node');
  $bundle = drush_get_option('bundle', '');
  $check_only = drush_get_option('check', FALSE);

  // If entity is provided, check that entity type exists.
  if (!empty($entity_type)) {
    $entity = entity_get_info($entity_type);
    if (empty($entity)) {
      drush_log('Error: ' . $entity_type . ' does not exist!', 'error');
      return;
    }
  }

  // Check that bundle exists.
  if (empty($bundle) || !array_key_exists($bundle, $entity['bundles'])) {
    drush_log('Error: Bundle ' . $bundle . ' does not exist!', 'error');
    return;
  }

  $args = func_get_args();

  // Iterate over each field spec.
  foreach ($args as $string) {

    list($name, $widget, $required, $label, $description) = explode(',', $string);

    // Check if field instance exists.
    $field = field_info_instance($entity_type, $name, $bundle);
    if (empty($field)) {
      drush_log('Error: Field instance ' . $name . ' does not exist in Bundle ' .
        $bundle, 'error');
      return;
    }

    // Check widget.
    if (!empty($widget)) {
      $info = field_info_widget_types($widget);
      if (empty($info)) {
        drush_log('Error: widget ' . $widget . ' is not defined for field name ' .
          $name . ' Field not updated.', 'error');
        return;
      }
      else {
        $field['widget']['type'] = $widget;
      }
    }
    // Check 'required'.
    if (!empty($required)) {
      if (in_array($required, array(0, 1))) {
        $field['required'] = $required;
      }
      else {
        drush_log('Warning: "required" (' . $required . ') is not 0 or 1. Property not changed.', 'warning');
      }
    }
    // Check label.
    if (!empty($label) && is_string($label)) {
      $field['label'] = $label;
    }
    // Check description.
    if (!empty($description) && is_string($description)) {
      $field['description'] = $description;
    }

    if (!$check_only) {
      try {
        drush_op('field_update_instance', $field);
      }
      catch (Exception $e) {
        drush_log('Error: Field instance *' . $name . '* can not be updated on Bundle *' .
          $bundle . '* !' . PHP_EOL . $e->getMessage(), 'error');
        return DRUSH_APPLICATION_ERROR;
      }

      drush_log('Field instance *' . $name . '* in Bundle ' . $bundle .
        ' successfully updated.', 'ok');
    }
    else {
      drush_log('Check mode: arguments are correct but no operation was performed.', 'ok');
    }

  }
}

/**
 * Remove a field from an entity/bundle pair.
 * drush efd contact_name
 */
function drush_entity_field_delete() {

  $check_only = drush_get_option('check', FALSE);

  $args = func_get_args();

  // Iterate over each field spec.
  foreach ($args as $name) {

    // Check if field exists.
    $info = field_info_fields();
    if (array_key_exists($name, $info)) {
      if (drush_confirm('ALL DATA CONTAINED IN THE FIELD ' . $name .
        ' WILL BE LOST! Are you sure ?', 0)) {
        if (!$check_only) {
          try {
            drush_op('field_delete_field', $name);
          }
          catch (Exception $e) {
            drush_log('Error: Field *' . $name . '* can not be deleted!' .
              PHP_EOL . $e->getMessage(), 'error');
            return DRUSH_APPLICATION_ERROR;
          }

          drush_log('Field ' . $name . ' successfully deleted.', 'ok');
        }
        else {
          drush_log('Check mode: arguments are correct but no operation was performed.', 'ok');
        }

      }
      else {
        drush_log('Command cancelled.', 'notice');
      }
    }
    else {
      drush_log('Info: Field ' . $name . ' does not exist!', 'notice');
    }
  }  // foreach

  // Purge all field information after a bulk deletion.
  field_purge_batch(1000);
}

/**
 * List all the available fields and widgets.
 * drush efl
 */
function drush_entity_field_list() {

  $widgets = field_info_widget_types();
  $field_list = array();

  foreach ($widgets as $key => $widget) {
    foreach ($widget['field types'] as $field) {
      $field_list[$field][] = $key;
    }
  }

  ksort($field_list);

  $headers = array(dt('FIELD TYPE') => array(dt('WIDGETS')));

  drush_print_table(
    drush_key_value_to_array_table(array_merge($headers, $field_list)),
    TRUE);
}

/**
 * Read an Excel file, creating the fields included in it.
 * drush efx
 */
function drush_entity_field_process_excel() {

  $args = func_get_args();
  $inputFileName = $args[0];

  if (empty($inputFileName)) {
    drush_log('Error: Please provide the Excel file to process as an argument', 'error');
    return DRUSH_APPLICATION_ERROR;
  }
  if (!file_exists($inputFileName)) {
    drush_log('Error: Excel file not found: ' . $inputFileName, 'error');
    return DRUSH_APPLICATION_ERROR;
  }

  $check_only = drush_get_option('check', FALSE);
  $PHPExcelLibPath = drush_get_option('PHPExcel', 'sites/all/libraries/PHPExcel');

  $PHPExcelLib = $PHPExcelLibPath. '/Classes/PHPExcel/IOFactory.php';

  if (file_exists($PHPExcelLib)) {
    require_once $PHPExcelLib;
  }
  else {
    drush_log('Error: Cannot load PHPExcel library from: ' . $PHPExcelLib, 'error');
    return DRUSH_APPLICATION_ERROR;
  }

  // Identify the version of $inputFileName.
  $inputFileType = PHPExcel_IOFactory::identify($inputFileName);

  // Create a new Reader of the type defined in $inputFileType.
  $objReader = PHPExcel_IOFactory::createReader($inputFileType);

  // Advise the Reader that we only want to load cell data.
  $objReader->setReadDataOnly(TRUE);

  // Load $inputFileName to a PHPExcel Object and get sheet data.
  $objPHPExcel = $objReader->load($inputFileName);
  $sheetData = $objPHPExcel->getActiveSheet()->toArray(NULL, TRUE, TRUE, TRUE);

  // Check the Excel file format: first row must have an specific format.
  // See https://www.drupal.org/files/efx_sample.xls.
  $header = array(
    'A' => 'ENABLED?',
    'B' => 'Entity',
    'C' => 'Bundle',
    'D' => 'Vocab',
    'E' => 'Name',
    'F' => 'Type',
    'G' => 'Label',
    'H' => 'Widget',
    'I' => 'Cardinality',
    'J' => 'Required?',
    'K' => 'Translatable?',
    );

  if ($header !== $sheetData[1]) {
    drush_log('Error: Excel file ' . $inputFileName . ' has a wrong format!', 'error');
    return DRUSH_APPLICATION_ERROR;
  }

  array_shift($sheetData);
  foreach ($sheetData as $parameters) {
    // Ignore disabled fields or comments
    if ($parameters['A'] != 1) {
      continue;
    }

    // Build the command.
    $arg = trim($parameters['E']) . ',' . trim($parameters['F']) . ',' .
           trim($parameters['G']) . ',' . trim($parameters['H']) . ',' .
           trim($parameters['I']) . ',' . trim($parameters['J']) . ',' .
           trim($parameters['K']);

    $options = array(
      'entity' => trim($parameters['B']),
      'bundle' => trim($parameters['C']),
    );

    if (trim($parameters['F']) == 'taxonomy_term_reference') {
      $options['vocab'] = trim($parameters['D']);
    }

    if ($check_only) {
      $options['check'] = TRUE;
    }

    drush_print('drush efc ' . $arg. ' --entity='. $options['entity'] .
      ' --bundle=' . $options['bundle']);

    if (drush_invoke_process('@self', 'efc', array($arg), $options) === FALSE) {
      drush_log('Error: command ' . $arg . ' failed!', 'error');
    }
  }
}

/**
 * Update an existing field definition.
 *
 * drush efcv vocabulary_name
 */
function drush_entity_field_create_vocabulary() {

  $check_only = drush_get_option('check', FALSE);

  $args = func_get_args();

  // Iterate over each field spec.
  foreach ($args as $vocab) {
    $voc = taxonomy_vocabulary_machine_name_load($vocab);
    if (is_object($voc)) {
      drush_log('Info: ' . $vocab . ' already exists.', 'ok');
    }
    else {
      $vocabulary = (object) array(
        'name' => $vocab,
        'description' => $vocab,
        'machine_name' => $vocab,
      );

      if (!$check_only) {
        $rc = taxonomy_vocabulary_save($vocabulary);
        if ($rc) {
          drush_log('Info: ' . $vocab . ' created.', 'ok');
        }
        else {
          drush_log('ERROR: ' . $vocab . ' could not be created.', 'error');
        }
      }
    }
  }
}
