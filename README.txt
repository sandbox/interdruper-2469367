Drush support for field CRUD operations over entities.

It supports the bulk creation of fields using an Excel file. This feature is
specially useful for the creation of content types or entities with a big
number of fields.

Installation
------------

This module is a Drush extension, so it may be installed at the
/home/user/.drush directory (Linux) or c:\Users\user\.drush under Windows.

cd /home/user/.drush
git clone --branch 7.x-1.x http://git.drupal.org/sandbox/interdruper/2469367.git
  drush_entity_field

Now you can use the module commands from your Drupal root dir.

*Bulk creation of fields using an Excel file

For using this feature, the PHPExcel library (https://phpexcel.codeplex.com/)
is required. Please download the latest available stable version of the library
and copy it to the sites/all/libraries/PHPExcel directory. The module uses
the library files directly, so the Libraries module is not required.

Usage
-----

Please use drush command --help for a detailed description of the options
for each command.

* Create a new field instance and attach it to a bundle:

    drush entity-field-create field_name, field_type, [field_label],
      [field_widget], [cardinality], [required], [translatable]
      [--entity={entity}] [--bundle={bundle}] [--vocab={vocab}] [--check]

        Alias: drush efc

* Update a field definition. Only cardinality and translatable properties
  can be changed:

    drush entity-field-update-definition field_name, [cardinality],
      [translatable] [--check]

        Alias: drush efud

* Update a field instance:

    drush entity-field-update-instance field_name, [field_widget], [required],
      [field_label], [field_description] [--entity={entity}]
      --bundle={bundle} [--check]

        Alias: drush efui

* Delete a field: its definition and all its instances:

    drush entity-field-delete field_name [--check]

        Alias: drush efd

* List all the available fields and widgets:

   drush entity-field-list

       Alias: drush efl

* Bulk creation of fields using an Excel file:

    PHPExcel library is required. The Excel file must have an specific format.
    Use efx_sample.xls as a model.

  drush entity-field-process-excel excelfilename [--check]
    [--PHPExcel={path to PHPExcel Library}]

      Alias: drush efx

Examples
--------

* Create a text field with no field instance associated

  drush efc text_multiple,text,,,-1

* Create an instance of the previous field in the 'page' bundle

  drush efc text_multiple,text,Campo1,text_textfield,,1,0 --entity=node --bundle=page
